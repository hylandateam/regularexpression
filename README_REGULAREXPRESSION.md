# 正则提取字符串

## 能力基本信息
+ 能力名称（中文）：正则提取字符串
+ 能力名称（英文）：RegularExpression
+ 版本：1.0.0
+ 能力入口类名：RegularExpression
+ 上线时间：2018-05-21  15:00:00


+ 作者：时金祥
+ 贡献者：
+ 编程语言：java

+ 权利所属公司：海纳
+ 合作公司：无
+ 授权范围：公开


+ 能力分类：普通能力
+ 工程分类：无
+ 能力级别：普通
+ 状态：启用

+ 标签：正则,regular,表达式

## 能力说明
+ 该Processor是从已知文本中按照正则表达式提取字符串，多个符合的按照 list形式输出


### 功能简介
+ 该Processor是从已知文本中按照正则表达式提取字符串，多个符合的按照 list形式输出

### 配置参数
RegularExpression 
| 参数名称 | 参数类型 | 是否必填 | 默认值 | 参数说明 |
| - | - | - | - | - |
| 要解析的字符串所在的字段 | 字符串 | 是 | 无  |  |
| 正则表达式| 字符串 | 是 | 无 |  |
| 解析后输出的字段 | 字符串 | 是 | 无 |  |

### 输入简介


### RegularExpression需要的数据流样例
配置信息：
| 要解析的字符串所在的字段 | content |
| 正则表达式| (?<=Image=).+?(?=;) | 
| 解析后输出的字段 | list |
输入的json字符串：
{
	"content":"Image=http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg;	reli=100;	pos=93;	src=http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg
Image=https://www.wifi588.net/3c906e2fa1a3b1477cdc61b29c0b46cc.jpg;	reli=100;	pos=93;	src=http://wx2.sinaimg.cn/mw690/6ae5db1fly1fqxvlm9o64j20j60bb3ys.jpg
"
}
### 输出简介

### RegularExpression输出流样例

{
	"list":	["http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg","https://www.wifi588.net/3c906e2fa1a3b1477cdc61b29c0b46cc.jpg"],
	"content":"Image=http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg;\treli=100;\tpos=93;\tsrc=http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg\nImage=https://www.wifi588.net/3c906e2fa1a3b1477cdc61b29c0b46cc.jpg;\treli=100;\tpos=93;\tsrc=http://wx2.sinaimg.cn/mw690/6ae5db1fly1fqxvlm9o64j20j60bb3ys.jpg\n"
}


### 相关能力
无

### 模型序号
无

### 远程group url
无

### 初始化配置
无

## 计费说明
+ 能力计费方式：按条数
+ 能力计费规则：
+ 关于计费的其他说明：

## 参考指标
+ 数据质量：暂无

+ 处理性能：

### 参考环境
暂无

## 附加说明
### 版本迭代记录
初始版本1.0.0

### 隐藏参数说明
无



