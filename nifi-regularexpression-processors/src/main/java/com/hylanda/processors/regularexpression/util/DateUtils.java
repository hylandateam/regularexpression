package com.hylanda.processors.regularexpression.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;




/**
 * 
 * @author shijinxiang
 * @date 2017年12月27日
 * @time 下午4:12:36
 */
public class DateUtils {
	
//	private static final DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * yyyyMMdd
	 */
	public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
	/**
	 * yyyy-MM-dd
	 */
	public static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
	
	
	
	
	/***
	 * 近n天库
	 * @param uni_release_date
	 * @param formatStr
	 * @return
	 */
	public static String[] getDates(int days) {
		
		if (days<=0) {
			return null;
		}
		String[] dayString = new String[days];
		
		Date date = new Date();
		Calendar cal =Calendar.getInstance();
		cal.setTime(date);
		
		for(int i=0;i<days;i++)
		{
			
			dayString[i] = sdf3.format(cal.getTime());//formatDate(cal.getTime(),sdf3);
			cal.add(Calendar.DATE, -1);
		}
		
		return dayString;
	}
	
	
}
