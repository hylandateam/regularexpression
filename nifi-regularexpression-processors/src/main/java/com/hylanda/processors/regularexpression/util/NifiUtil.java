package com.hylanda.processors.regularexpression.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.stream.io.StreamUtils;

public class NifiUtil {
	/**
	 * 获取content中的内容
	 * 
	 * @param session
	 * @param flowFile
	 * @return
	 */
	public static String getContent(final ProcessSession session, final FlowFile flowFile) {
		// Read the SQL from the FlowFile's content
		final byte[] buffer = new byte[(int) flowFile.getSize()];
		session.read(flowFile, new InputStreamCallback() {
			@Override
			public void process(final InputStream in) throws IOException {
				StreamUtils.fillBuffer(in, buffer);
			}
		});

		// Create the PreparedStatement to use for this FlowFile.
		final String content = new String(buffer, StandardCharsets.UTF_8);
		return content;
	}
}
