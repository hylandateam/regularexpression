package com.hylanda.processors.regularexpression.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class RegularExtressionUtil {

	public static List<String> parsering(String source,String regular)
	{
		List<String> list = new ArrayList<>();
		
		Pattern pattern = Pattern.compile(regular);
		
		Matcher matcher = pattern.matcher(source);
		
		while (matcher.find()) {
			String temp = matcher.group();
			if (!list.contains(temp)) {
				list.add(temp);
			}
			
		}
		return list;
	}
	public static void main(String[] args) {
		
		String source = "Image=http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg;	reli=100;	pos=93;	src=http://wx4.sinaimg.cn/mw690/6ae5db1fly1fqxvknb40yj20jo0cijwj.jpg"
				+
"Image=http://wx2.sinaimg.cn/mw690/6ae5db1fly1fqxvlm9o64j20j60bb3ys.jpg;	reli=100;	pos=93;	src=http://wx2.sinaimg.cn/mw690/6ae5db1fly1fqxvlm9o64j20j60bb3ys.jpg";
		String regular = "(?<=Image=).+?(?=;)";
		List<String> list = parsering(source, regular);
		
		/*JSONObject jsonObject = new JSONObject();
		jsonObject.put("list", list);
		String result = jsonObject.toJSONString();
		
		List<String> list2 = //(List<String>) jsonObject.get("list");
		jsonObject.getObject("list", List.class);
		for (int i = 0; i < list2.size(); i++) {
			System.out.println(list2.get(i));
		}*/
	}
}
