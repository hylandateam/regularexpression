/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hylanda.processors.regularexpression;

import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.flowfile.attributes.CoreAttributes;
import org.apache.nifi.logging.ComponentLog;
import org.apache.commons.io.IOUtils;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;

import com.alibaba.fastjson.JSONObject;
import com.hylanda.processors.regularexpression.util.GenUrlCrc64;
import com.hylanda.processors.regularexpression.util.NifiUtil;
import com.hylanda.processors.regularexpression.util.RegularExtressionUtil;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Tags({"正则,regular,表达式"})
@CapabilityDescription("按照正则表达式来解析某个字符串")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="", description="")})
public class RegularExpression extends AbstractProcessor {

	public static final PropertyDescriptor SOURCE_FIELD = new PropertyDescriptor.Builder().name("SOURCE_FIELD")
			.displayName("要解析的字符串所在的字段").required(true).addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
			.build();
    public static final PropertyDescriptor REGULAR_EXPRESSION = new PropertyDescriptor.Builder().name("REGULAR_EXPRESSION")
			.displayName("正则表达式").required(true).addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
			.build();
    public static final PropertyDescriptor RESULT_FIELD = new PropertyDescriptor.Builder().name("RESULT_FIELD")
			.displayName("解析后输出的字段").required(true).addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
			.build();
    public static final Relationship SUCCESS = new Relationship.Builder()
            .name("SUCCESS")
            .description("SUCCESS")
            .build();
    public static final Relationship FAILURE = new Relationship.Builder()
            .name("FAILURE")
            .description("FAILURE")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(SOURCE_FIELD);
        descriptors.add(REGULAR_EXPRESSION);
        descriptors.add(RESULT_FIELD);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(SUCCESS);
        relationships.add(FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        long startTime = System.currentTimeMillis();
        if ( flowFile == null ) {
            return;
        }
       
        String sourceField = null;
        try {
        	sourceField =context.getProperty(SOURCE_FIELD).getValue().trim();
		} catch (Exception e) {
			// TODO: handle exception
		}
        String regularExpression = null;
        try {
        	regularExpression =context.getProperty(REGULAR_EXPRESSION).getValue().trim();
		} catch (Exception e) {
			// TODO: handle exception
		}
        String resultField = null;
        try {
        	resultField =context.getProperty(RESULT_FIELD).getValue().trim();
		} catch (Exception e) {
			// TODO: handle exception
		}
        
        String content = NifiUtil.getContent(session, flowFile);
        
        ComponentLog log = getLogger();
      
        if (content==null) {
        	log.info("content is null");
        	session.transfer(flowFile, FAILURE);
			return;
		}
        //log.info("GetATags content :"+content);
        JSONObject jsonObject = JSONObject.parseObject(content);
        String source_content = null;
        source_content = jsonObject.getString(sourceField);
        if (source_content == null) {
        	session.transfer(flowFile, FAILURE);
			return;
		}
        List<String> list = RegularExtressionUtil.parsering(source_content, regularExpression);
        
        long endTime = System.currentTimeMillis();
        jsonObject.put("list", list);
        jsonObject.put("elapsedTime_regularExpression", endTime-startTime);
        flowFile = session.write(flowFile, (in, out) -> {
			try (OutputStream outputStream = new BufferedOutputStream(out)) {				
				outputStream.write(IOUtils.toByteArray(jsonObject.toJSONString()));
			}
		});
        String session_id = flowFile.getAttribute(CoreAttributes.UUID.key());
        
        String uuid = GenUrlCrc64.GenCrc64(UUID.randomUUID().toString());
       // String session_id = flowFile.getAttribute(CoreAttributes.UUID.key());
        String instance_id = getIdentifier();
        JSONObject logJSON = new JSONObject();
        JSONObject msgJSON = new JSONObject();
        
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(date);
        msgJSON.put("time", time);
        msgJSON.put("status", "success");
        
        logJSON.put("msg", msgJSON);
        logJSON.put("session_id", session_id);
        logJSON.put("uuid", uuid);
        logJSON.put("instance_id", instance_id);
        getLogger().billing(session_id);
        getLogger().info(logJSON.toJSONString());
        session.transfer(flowFile, SUCCESS);
        
    }
    
}
